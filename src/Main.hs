module Main where

main :: IO ()
main = do
  putStrLn "please input sum"
  s <- readLn
  putStrLn "please input deposit persentage"
  p <- readLn
  putStrLn "please input expected multiplier"
  m <- readLn
  putStrLn ("expected time to achieve multiplier: " 
    ++ show (logBase (1 + p / 100) m)
    ++ " years\nsum after 2 years: "
    ++ show (s * (1 + p / 100) ** 2))
